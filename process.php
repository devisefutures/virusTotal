<?php
// begin config
$virustotal_api_key = 'be76625af60fce19262b9e933937d57b28485dce64bb77b0efeb1d7849aa5e7e';
$virustotal_get_url = 'http://www.virustotal.com/vtapi/v2/domain/report';
// end config

$scan_domain = 'www.google.com';

// Get cURL resource
$curl = curl_init();
// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => "$virustotal_get_url?apikey=$virustotal_api_key&domain=$scan_domain",
    CURLOPT_USERAGENT => 'CyberIntell'
));
// Send the request & save response to $result
$result = curl_exec($curl);


$status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
print("status = $status_code\n");
if ($status_code == 200) { // OK
  $js = json_decode($result, true);
  print_r($js);
} else {  // Error occured
  print($result);
}

// Close request to clear up some resources
curl_close ($curl);

?>
